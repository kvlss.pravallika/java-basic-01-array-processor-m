package com.epam.java_basic.array_processor;

/**
 * Useful methods for array processing
 */
public class ArrayProcessor {

    public int[] swapMaxNegativeAndMinPositiveElements(int[] input) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public int countSumOfElementsOnEvenPositions(int[] input) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public int[] replaceEachNegativeElementsWithZero(int[] input) {
        for (int i = 0; i < input.length; i++) {
            if (input[i] < 0) {
                input[i] = 0;
            }
        }
        return input;
    }

    public int[] multiplyByThreeEachPositiveElementStandingBeforeNegative(int[] input) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public float calculateDifferenceBetweenAverageAndMinElement(int[] input) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

    public int[] findSameElementsStandingOnOddPositions(int[] input) {
        throw new UnsupportedOperationException("You need to implement this method");
    }

}
